﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PostalCodes.Models;

namespace DataStructure
{
    public static class Utils
    {       
        public static string ArrayToString<T>(this T[] inArray)
        {
            return inArray.Select(i => string.Format("{0};", i)).Aggregate((cur, next) =>
            {
                return cur += next;
            });
        }

        public static bool Match<T>(this T[] inArray, T[] expected)
        {
            if (inArray == null || expected == null)
            {
                throw new ArgumentNullException("inArray");
            }

            if (inArray.Length != expected.Length)
            {
                return false;
            }

            for (int counter = 0; counter < inArray.Length; counter++)
            {
                if (!inArray[counter].Equals(expected[counter]))
                {
                    return false;
                }
            }

            return true;
        }         

        public static void Wrapper<T>(double[] results, Func<T[]> doThis, T[] expected)
        {            
            var iterations = results.Length;

            for (int iterationCounter = 0; iterationCounter < iterations; iterationCounter++)
            {                
                var sw = Stopwatch.StartNew();
                T[] postalCodes = doThis();                
                sw.Stop();

                if (expected != null)
                {
                    if (!postalCodes.Match(expected))
                    {
                        throw new Exception("Output is incorrect");
                    }
                }

                results[iterationCounter] = sw.Elapsed.TotalSeconds / iterations;
            }                                        
        }        
    }
}
