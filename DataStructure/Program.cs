﻿#define BUILD
namespace DataStructure
{
    using System;
    using System.Windows.Forms;

    class Program
    {
        public static int iterations = 2;

        [STAThread]
        static void Main(string[] args)
        {
#if BUILD
            //DataPackagers.PreBuild();
            DataPackagers.PreBuildCustom();
            DataPackagers.PreBuildToFile();
            //DataPackagers.RebuildData();
            //DataPackagers.BuildFixedWidth();            
            //DataPackagers.BuildSingleLine();
#else
            var oldResults = new double[iterations];
            var newResults = new double[iterations];
            var tweakedResults = new double[iterations];
            var base64results = new double[iterations];
            var binaryPure = new double[iterations];
            var binaryPureFast = new double[iterations];
            var structsWay = new double[iterations];
            var structsAndConsts = new double[iterations];
            var structsConstAndPrebuilt = new double[iterations];
            var structsConstPreBuiltSingleSplit = new double[iterations];
            var structsConstPreBuiltFixedWidth = new double[iterations];
            var structsConstPreBuiltCustomFixedWidth = new double[iterations];
            var oldwayData = Tests.OldWay();
            var structsData = Tests.Structs();

            Tests.MeasureOldWay(oldResults);
            Tests.NewWay(newResults, oldwayData);
            Tests.TweakedWay(tweakedResults, oldwayData);
            Tests.Base64Way(base64results, oldwayData);
            Tests.ParseBinaryPure(binaryPure, oldwayData);
            Tests.ParseBinaryPureFast(binaryPureFast, oldwayData);
            Tests.MeasureStructs(structsWay);
            Tests.StructsAndConsts(structsAndConsts, structsData);
            Tests.StructsConstAndPrebuilt(structsConstAndPrebuilt, structsData);
            Tests.StructsConstPreBuiltSingleSplit(structsConstPreBuiltSingleSplit, structsData);
            Tests.StructsConstPreBuiltFixedWidth(structsConstPreBuiltFixedWidth, structsData);

            var line1 = oldResults.ArrayToString();
            var line2 = newResults.ArrayToString();
            var line3 = tweakedResults.ArrayToString();
            var line4 = base64results.ArrayToString();
            var line5 = binaryPure.ArrayToString();
            var line6 = binaryPureFast.ArrayToString();
            var line7 = structsWay.ArrayToString();
            var line8 = structsAndConsts.ArrayToString();
            var line9 = structsConstAndPrebuilt.ArrayToString();
            var line10 = structsConstPreBuiltSingleSplit.ArrayToString();
            var line11 = structsConstPreBuiltFixedWidth.ArrayToString();

            Clipboard.SetText(line1 + Environment.NewLine +
                line2 + Environment.NewLine +
                line3 + Environment.NewLine +
                line4 + Environment.NewLine +
                line5 + Environment.NewLine +
                line6 + Environment.NewLine +
                line7 + Environment.NewLine +
                line8 + Environment.NewLine +
                line9 + Environment.NewLine +
                line10 + Environment.NewLine +
                line11 + Environment.NewLine);
#endif
        }
    }
}
