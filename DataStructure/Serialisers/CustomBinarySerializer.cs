﻿// source: http://www.eugenedotnet.com/2010/12/windows-phone-7-serialization-binary-serialization/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace DataStructure
{
    public class CustomBinarySerializer
    {
        private List<PropertyInfo> serializableProperties = new List<PropertyInfo>();
        private Type serializableObjectType;

        public CustomBinarySerializer(Type objectType)
        {
            serializableObjectType = objectType;
            serializableProperties = GetMarkedProperties(objectType);
        }

        private List<PropertyInfo> GetMarkedProperties(Type type)
        {
            return (from property in type.GetProperties()
                    select property).ToList();
        }

        public void WriteObject(Stream stream, object graph)
        {
            if (stream == null || graph == null || graph as IEnumerable == null)
            {
                return;
            }

            var collection = graph as IEnumerable;

            var bw = new BinaryWriter(stream);

            foreach (var item in collection)
            {
                foreach (var pi in serializableProperties)
                {
                    var value = pi.GetValue(item, null);

                    if (pi.PropertyType == typeof(string))
                    {
                        bw.Write(value as string ?? string.Empty);
                        continue;
                    }

                    if (pi.PropertyType == typeof(int))
                    {
                        bw.Write((int)value);
                        continue;
                    }

                    if (pi.PropertyType == typeof(Nullable<int>))
                    {
                        var v = value as Nullable<int>;
                        bw.Write(v.HasValue ? v.Value : 0);
                        continue;
                    }

                    throw new NotSupportedException("Data type is not supported");
                }
            }
        }

        public T[] ReadObject<T>(Stream stream)
        {
            if (stream == null)
            {
                return null;
            }


            var result = new List<T>();

            var br = new BinaryReader(stream);

            while (true)
            {
                try
                {
                    T deserializedObject = (T)Activator.CreateInstance(typeof(T));

                    foreach (var pi in serializableProperties)
                    {
                        if (pi.PropertyType == typeof(string))
                        {
                            pi.SetValue(deserializedObject, br.ReadString(), null);
                            continue;
                        }

                        if (pi.PropertyType == typeof(int) || pi.PropertyType == typeof(Nullable<int>))
                        {
                            pi.SetValue(deserializedObject, br.ReadInt32(), null);
                            continue;
                        }

                        throw new NotSupportedException("Data type is not supported");
                    }

                    result.Add(deserializedObject);
                }
                catch (EndOfStreamException)
                {
                    break;
                }
            }

            return result.ToArray();
        }
    }
}
