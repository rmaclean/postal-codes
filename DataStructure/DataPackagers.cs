﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using DataStructure.Resources;
using PostalCodes.Models;

namespace DataStructure
{
    public static class DataPackagers
    {
        public static void PreBuildToFile()
        {
            PostalCode[] postalCodes;

            var lines = Data.RAW.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            postalCodes = new PostalCode[lines.Length];
            string[] line;
            for (int counter = 0; counter < lines.Length; counter++)
            {
                line = lines[counter].Split(';');
                postalCodes[counter] = new PostalCode(line[0], line[1], line[2], line[3]);
            }

            var ser = new CustomBinarySerializer(typeof(PostalCode));
            using (var stream = new FileStream(@"C:\Users\Robert.1235791315\Desktop\postalcodes.dat", FileMode.CreateNew, FileAccess.ReadWrite))
            {
                ser.WriteObject(stream, postalCodes);
                stream.Flush();
            }
        }

        public static void RebuildData()
        {
            PostalCode[] postalCodes;

            var lines = Data.RAW.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            postalCodes = new PostalCode[lines.Length];
            string[] line;
            for (int counter = 0; counter < lines.Length; counter++)
            {
                line = lines[counter].Split(';');
                postalCodes[counter] = new PostalCode(line[0], line[1], line[2], line[3]);
            }

            var result = string.Empty;

            foreach (var item in postalCodes)
            {
                result += string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9}",
                    item.BoxCode, item.City, item.DisplayLocation,
                    item.NumericBoxCode, item.NumericStreetCode, item.SearchableCity,
                    item.SearchableTown, item.StreetCode, item.Town,
                    Environment.NewLine);
            }

            Clipboard.SetText(result);
        }

        public static void BuildFixedWidth()
        {
            PostalCode[] postalCodes;

            var lines = Data.RAW.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            postalCodes = new PostalCode[lines.Length];
            string[] line;
            for (int counter = 0; counter < lines.Length; counter++)
            {
                line = lines[counter].Split(';');
                postalCodes[counter] = new PostalCode(line[0], line[1], line[2], line[3]);
            }

            var result = string.Empty;

            var longestCity = (from p in postalCodes
                               select p.City.Length).Max();

            var longestLocation = (from p in postalCodes
                                   select p.DisplayLocation.Length).Max();

            var longestSearchableCity = (from p in postalCodes
                                         select p.SearchableCity.Length).Max();

            var longestSearchableTown = (from p in postalCodes
                                         select p.SearchableTown.Length).Max();

            var longestTown = (from p in postalCodes
                               select p.Town.Length).Max();

            var widthGuide = string.Format("4,{0},{1},4,4,{2},{3},4,{4}{5}",
                longestCity, longestLocation, longestSearchableCity, longestSearchableTown, longestTown, Environment.NewLine);

            foreach (var item in postalCodes)
            {
                result += string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}",
                    item.BoxCode.PadRight(4), item.City.PadRight(longestCity),
                    item.DisplayLocation.PadRight(longestLocation), item.NumericBoxCode.ToString().PadRight(4),
                    item.NumericStreetCode.ToString().PadRight(4), item.SearchableCity.PadRight(longestSearchableCity),
                    item.SearchableTown.PadRight(longestSearchableTown), item.StreetCode.PadRight(4),
                    item.Town.PadRight(longestTown), Environment.NewLine);
            }

            Clipboard.SetText(widthGuide + result);
        }

        public static void PreBuild()
        {
            PostalCode[] postalCodes;

            var lines = Data.RAW.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            postalCodes = new PostalCode[lines.Length];
            string[] line;
            for (int counter = 0; counter < lines.Length; counter++)
            {
                line = lines[counter].Split(';');
                postalCodes[counter] = new PostalCode(line[0], line[1], line[2], line[3]);
            }

            var serialiser = new XmlSerializer(postalCodes.GetType());
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            var sb = new StringBuilder();
            using (var writer = new StringWriter(sb))
            {
                serialiser.Serialize(writer, postalCodes, ns);
                Clipboard.SetText(writer.ToString());
            }
        }

        public static void PreBuildCustom()
        {
            PostalCode[] postalCodes;

            var lines = Data.RAW.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            postalCodes = new PostalCode[lines.Length];
            string[] line;
            for (int counter = 0; counter < lines.Length; counter++)
            {
                line = lines[counter].Split(';');
                postalCodes[counter] = new PostalCode(line[0], line[1], line[2], line[3]);
            }

            var ser = new CustomBinarySerializer(typeof(PostalCode));
            using (var stream = new MemoryStream())
            {
                ser.WriteObject(stream, postalCodes);
                stream.Flush();
                stream.Position = 0;
                Clipboard.SetText(Convert.ToBase64String(stream.ToArray()));
            }
        }

        internal static void BuildSingleLine()
        {
            PostalCode[] postalCodes;

            var lines = Data.RAW.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            postalCodes = new PostalCode[lines.Length];
            string[] line;
            for (int counter = 0; counter < lines.Length; counter++)
            {
                line = lines[counter].Split(';');
                postalCodes[counter] = new PostalCode(line[0], line[1], line[2], line[3]);
            }

            var result = string.Empty;

            foreach (var item in postalCodes)
            {
                result += string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};",
                    item.BoxCode, item.City, item.DisplayLocation,
                    item.NumericBoxCode, item.NumericStreetCode, item.SearchableCity,
                    item.SearchableTown, item.StreetCode, item.Town);
            }

            Clipboard.SetText(result);
        }
    }

}
