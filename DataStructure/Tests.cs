﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using DataStructure.Resources;
using Microsoft.VisualBasic.FileIO;
using PostalCodes.Models;

namespace DataStructure
{
    public static class Tests
    {
        public static void StructsConstPreBuiltFixedWidth(double[] results, PostalCodeStruct[] expected)
        {
            Utils.Wrapper(results, () =>
            {
                using (var reader = new StringReader(PreBuildFixedWidth.Data))
                {
                    var parser = new TextFieldParser(reader);
                    parser.TextFieldType = FieldType.FixedWidth;
                    parser.SetFieldWidths(4, 20, 51, 4, 4, 20, 40, 4, 40);
                    var resultSet = new List<PostalCodeStruct>(16737);
                    while (!parser.EndOfData)
                    {
                        var fields = parser.ReadFields();
                        var item = new PostalCodeStruct();
                        item.BoxCode = fields[0];
                        item.City = fields[1];
                        item.DisplayLocation = fields[2];
                        if (!string.IsNullOrWhiteSpace(fields[3]))
                        {
                            item.NumericBoxCode = Convert.ToInt32(fields[3]);
                        }

                        if (!string.IsNullOrWhiteSpace(fields[4]))
                        {
                            item.NumericStreetCode = Convert.ToInt32(fields[4]);
                        }

                        item.SearchableCity = fields[5];
                        item.SearchableTown = fields[6];
                        item.StreetCode = fields[7];
                        item.Town = fields[8];

                        resultSet.Add(item);
                    }

                    return resultSet.ToArray();
                }
            }, expected);
        }

        public static void StructsConstPreBuiltSingleSplit(double[] results, PostalCodeStruct[] expected)
        {
            Utils.Wrapper(results, () =>
            {
                var lines = SingleLineData.Data.Split(new[] { ";" }, StringSplitOptions.None);
                var postalCodes = new PostalCodeStruct[lines.Length / 9];
                var itemCounter = 0;
                for (int counter = 0; counter < lines.Length - 9; counter = counter + 9)
                {
                    postalCodes[itemCounter].BoxCode = lines[counter];
                    postalCodes[itemCounter].City = lines[counter + 1];
                    postalCodes[itemCounter].DisplayLocation = lines[counter + 2];
                    if (!string.IsNullOrWhiteSpace(lines[counter + 3]))
                    {
                        postalCodes[itemCounter].NumericBoxCode = Convert.ToInt32(lines[counter + 3]);
                    }

                    if (!string.IsNullOrWhiteSpace(lines[counter + 4]))
                    {
                        postalCodes[itemCounter].NumericStreetCode = Convert.ToInt32(lines[counter + 4]);
                    }
                    postalCodes[itemCounter].SearchableCity = lines[counter + 5];
                    postalCodes[itemCounter].SearchableTown = lines[counter + 6];
                    postalCodes[itemCounter].StreetCode = lines[counter + 7];
                    postalCodes[itemCounter].Town = lines[counter + 8];

                    itemCounter++;
                }

                return postalCodes;
            }, expected);
        }

        public static void StructsConstAndPrebuilt(double[] results, PostalCodeStruct[] expected)
        {
            Utils.Wrapper(results, () =>
            {
                var lines = Prebuilt.Data.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                var postalCodes = new PostalCodeStruct[lines.Length];
                string[] line;
                for (int counter = 0; counter < lines.Length; counter++)
                {
                    line = lines[counter].Split(';');
                    postalCodes[counter].BoxCode = line[0];
                    postalCodes[counter].City = line[1];
                    postalCodes[counter].DisplayLocation = line[2];
                    if (!string.IsNullOrWhiteSpace(line[3]))
                    {
                        postalCodes[counter].NumericBoxCode = Convert.ToInt32(line[3]);
                    }

                    if (!string.IsNullOrWhiteSpace(line[4]))
                    {
                        postalCodes[counter].NumericStreetCode = Convert.ToInt32(line[4]);
                    }
                    postalCodes[counter].SearchableCity = line[5];
                    postalCodes[counter].SearchableTown = line[6];
                    postalCodes[counter].StreetCode = line[7];
                    postalCodes[counter].Town = line[8];
                }

                return postalCodes;
            }, expected);
        }


        public static void StructsAndConsts(double[] results, PostalCodeStruct[] expected)
        {
            Utils.Wrapper(results, () =>
            {
                var lines = Class2.Data.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                var postalCodes = new PostalCodeStruct[lines.Length];
                string[] line;
                for (int counter = 0; counter < lines.Length; counter++)
                {
                    line = lines[counter].Split(';');
                    postalCodes[counter].SetValues(line[0], line[1], line[2], line[3]);
                }
                return postalCodes;
            }, expected);
        }

        public static PostalCodeStruct[] Structs()
        {
            var lines = Data.RAW.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            var postalCodes = new PostalCodeStruct[lines.Length];
            string[] line;
            for (int counter = 0; counter < lines.Length; counter++)
            {
                line = lines[counter].Split(';');
                postalCodes[counter].SetValues(line[0], line[1], line[2], line[3]);
            }
            return postalCodes;
        }

        public static void MeasureStructs(double[] results)
        {
            Utils.Wrapper(results, () =>
            {
                return Structs();
            }, null);
        }

        public static void ParseBinaryPureFast(double[] results, PostalCode[] expected)
        {
            Utils.Wrapper(results, () =>
            {
                var ser = new FastBinarySerializer<PostalCode>(17000);
                using (var stream = new MemoryStream(BinaryPure.postalcodes))
                {
                    stream.Flush();
                    stream.Position = 0;
                    return ser.ReadObject(stream);
                }
            }, expected);
        }

        public static void ParseBinaryPure(double[] results, PostalCode[] expected)
        {
            Utils.Wrapper(results, () =>
            {
                var ser = new CustomBinarySerializer(typeof(PostalCode));
                using (var stream = new MemoryStream(BinaryPure.postalcodes))
                {
                    stream.Flush();
                    stream.Position = 0;
                    return ser.ReadObject<PostalCode>(stream);
                }
            }, expected);
        }

        public static void Base64Way(double[] results, PostalCode[] expected)
        {
            Utils.Wrapper(results, () =>
            {
                var doc = Convert.FromBase64String(Hmm.Data);
                var ser = new CustomBinarySerializer(typeof(PostalCode));
                using (var stream = new MemoryStream(doc))
                {
                    stream.Flush();
                    stream.Position = 0;
                    return ser.ReadObject<PostalCode>(stream);
                }
            }, expected);
        }

        public static void NewWay(double[] results, PostalCode[] expected)
        {
            Utils.Wrapper(results, () =>
            {
                var doc = new XmlDocument();
                doc.LoadXml(Data.Parsed);
                var reader = new XmlNodeReader(doc.DocumentElement);
                var ser = new XmlSerializer(typeof(PostalCode[]));
                return (PostalCode[])ser.Deserialize(reader);
            }, expected);
        }

        public static void TweakedWay(double[] results, PostalCode[] expected)
        {
            Utils.Wrapper(results, () =>
            {
                var doc = new XmlDocument();
                doc.LoadXml(TweakedData.String1);
                var reader = new XmlNodeReader(doc.DocumentElement);
                var ser = new XmlSerializer(typeof(PostalCode[]));
                return (PostalCode[])ser.Deserialize(reader);
            }, expected);
        }

        public static PostalCode[] OldWay()
        {

            var lines = Data.RAW.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            var postalCodes = new PostalCode[lines.Length];
            string[] line;
            for (int counter = 0; counter < lines.Length; counter++)
            {
                line = lines[counter].Split(';');
                postalCodes[counter] = new PostalCode(line[0], line[1], line[2], line[3]);
            }

            return postalCodes;
        }

        public static void MeasureOldWay(double[] results)
        {
            Utils.Wrapper(results, () =>
            {
                return OldWay();
            }, null);

        }


    }
}
