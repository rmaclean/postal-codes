﻿
namespace PostalCodes.Models
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Runtime.Serialization;


    /// <summary></summary>
    public struct PostalCodeStruct
    {
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int valueStorage = 0;
            object objectValue = null;
            foreach (PropertyInfo property in typeof(PostalCodeStruct).GetProperties())
            {
                objectValue = property.GetValue(this, null);
                if (objectValue != null)
                {
                    valueStorage += objectValue.GetHashCode();
                }
            }

            return valueStorage;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is PostalCodeStruct))
                return false;

            return Equals((PostalCodeStruct)obj);
        }

        /// <summary>
        /// Equalses the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        public bool Equals(PostalCodeStruct other)
        {
            if (other.NumericStreetCode.HasValue && this.NumericStreetCode.HasValue &&
                other.NumericStreetCode.Value != this.NumericStreetCode.Value)
            {
                return false;
            }

            if (other.NumericBoxCode.HasValue && this.NumericBoxCode.HasValue &&
                other.NumericBoxCode.Value != this.NumericBoxCode.Value)
            {
                return false;
            }

            return other.SearchableTown.Equals(this.SearchableTown, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator ==(PostalCodeStruct first, PostalCodeStruct second)
        {
            return first.Equals(second);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator !=(PostalCodeStruct first, PostalCodeStruct second)
        {
            return !first.Equals(second);
        }

        public string Town { get; set; }
        public string BoxCode { get; set; }
        public string StreetCode { get; set; }
        public string City { get; set; }
        public string SearchableTown { get; set; }
        public string SearchableCity { get; set; }
        public string DisplayLocation { get; set; }
        public int? NumericStreetCode { get; set; }
        public int? NumericBoxCode { get; set; }

        public void SetValues(string town, string boxCode, string streetCode, string city)
        {
            Town = town;
            SearchableTown = town.ToLowerInvariant();
            BoxCode = boxCode;
            StreetCode = streetCode;
            City = city;
            SearchableCity = string.IsNullOrWhiteSpace(city) ? string.Empty : city.ToLowerInvariant();
            DisplayLocation = string.Format(CultureInfo.CurrentCulture, "{0} {1}", town, string.IsNullOrWhiteSpace(city) ? string.Empty : string.Format(CultureInfo.CurrentCulture, "({0})", city));
            var parsedStreetCode = 0;
            if (Int32.TryParse(streetCode, out parsedStreetCode))
            {
                NumericStreetCode = parsedStreetCode;
            }

            var parsedBoxCode = 0;
            if (Int32.TryParse(streetCode, out parsedBoxCode))
            {
                NumericBoxCode = parsedBoxCode;
            }
        }
    }
}
