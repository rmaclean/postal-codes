﻿/// Original Source: http://weblogs.asp.net/jdanforth/archive/2010/09/17/silverlight-watermark-textbox-behavior.aspx
/// Original license: Public Domain
/// Permission to use: https://twitter.com/#!/johandanforth/status/192536139154997248

namespace PostalCodes.Behaviours
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Interactivity;
    using System.Windows.Media;

    public class Watermark : Behavior<TextBox>
    {
        private bool hasWatermark;
        private Brush originalForegroundColour;

        public String Text { get; set; }
        public Brush Foreground { get; set; }

        protected override void OnAttached()
        {
            originalForegroundColour = AssociatedObject.Foreground;

            AssociatedObject.GotFocus += GotFocus;
            AssociatedObject.LostFocus += LostFocus;

            base.OnAttached();
            SetWatermarkText();
        }

        private void LostFocus(object sender, RoutedEventArgs e)
        {
            SetWatermarkText();
        }

        private void GotFocus(object sender, RoutedEventArgs e)
        {
            if (hasWatermark)
            {
                RemoveWatermarkText();
            }
        }

        private void RemoveWatermarkText()
        {
            AssociatedObject.Foreground = originalForegroundColour;
            AssociatedObject.Text = string.Empty;
            hasWatermark = false;
        }

        public void SetWatermarkText()
        {
            if (string.IsNullOrWhiteSpace(AssociatedObject.Text))
            {
                AssociatedObject.Foreground = Foreground;
                AssociatedObject.Text = Text;
                hasWatermark = true;
            }
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.GotFocus -= GotFocus;
            AssociatedObject.LostFocus -= LostFocus;
        }
    }
}
