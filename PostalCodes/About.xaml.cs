﻿

namespace PostalCodes
{
    using Microsoft.Phone.Controls;
    using AtomicPhoneMVVM;

    public partial class About : PhoneApplicationPage
    {
        public About()
        {
            InitializeComponent();
            this.BindData<ViewModels.About>();
        }
    }
}