﻿
namespace PostalCodes.Converters
{
    using System;
    using System.Collections;
    using System.Windows;
    using System.Windows.Data;

    public class CollectionToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var list = value as ICollection;
            if (list != null)
            {
                return list.Count > 0 ? Visibility.Visible : Visibility.Collapsed;
            }

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
