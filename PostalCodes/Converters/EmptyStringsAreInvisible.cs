﻿

namespace PostalCodes.Converters
{
    using System;
    using System.Windows;
    using System.Windows.Data;

    public class EmptyStringsAreInvisible : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var input = (string)value;
            return string.IsNullOrWhiteSpace(input) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
