﻿
namespace PostalCodes.Models
{
    using System;
    using System.Device.Location;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using Newtonsoft.Json;

    public class GoogleAddressResolver
    {
        //"http://maps.googleapis.com/maps/api/geocode/json?sensor=true&latlng=40.714224,-73.961452";
        private const string baseUrl = "http://maps.googleapis.com/maps/api/geocode/json?sensor=true&latlng=";

        public event Action<CivicAddress> AddressResolutionComplete;

        public void Resolve(GeoCoordinate location)
        {
            var url = new Uri(string.Format(CultureInfo.InvariantCulture, "{0}{1},{2}", baseUrl, location.Latitude, location.Longitude));
            var client = new WebClient();
            client.DownloadStringCompleted += (s, e) =>
                {
                    if (e.Cancelled || e.Error != null || string.IsNullOrWhiteSpace(e.Result))
                    {
                        return;
                    }

                    if (AddressResolutionComplete == null)
                    {
                        return;
                    }

                    GoogleGeolocation addresses = null;
                    try
                    {
                        addresses = new GoogleGeolocation(e.Result);
                    }
                    catch (JsonReaderException)
                    {
                        return;
                    }

                    var googleAddress = (from a in addresses.Results
                                         where a.Types.Contains("street_address")
                                         select a).FirstOrDefault();

                    if (googleAddress == null)
                    {
                        return;
                    }


                    var civic = new CivicAddress()
                    {
                        AddressLine1 = googleAddress.AddressComponents.FirstOrDefault(_ => _.Types.Contains("street_number")).LongName,
                        AddressLine2 = googleAddress.AddressComponents.FirstOrDefault(_ => _.Types.Contains("route")).LongName,
                        City = googleAddress.AddressComponents.FirstOrDefault(_ => _.Types.Contains("locality")).LongName,
                        CountryRegion = googleAddress.AddressComponents.FirstOrDefault(_ => _.Types.Contains("country")).LongName,
                        PostalCode = googleAddress.AddressComponents.FirstOrDefault(_ => _.Types.Contains("postal_code")).LongName,
                        StateProvince = googleAddress.AddressComponents.FirstOrDefault(_ => _.Types.Contains("administrative_area_level_1")).LongName
                    };

                    AddressResolutionComplete(civic);

                };

            client.DownloadStringAsync(url);
        }
    }
}
