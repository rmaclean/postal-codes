﻿
namespace PostalCodes.Models
{
    using System;
    using System.Globalization;
    using System.Runtime.Serialization;
    using System.Reflection;

    /// <summary></summary>
    public struct PostalCode
    {
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int valueStorage = 0;
            object objectValue = null;
            foreach (PropertyInfo property in typeof(PostalCode).GetProperties())
            {
                objectValue = property.GetValue(this, null);
                if (objectValue != null)
                {
                    valueStorage += objectValue.GetHashCode();
                }
            }

            return valueStorage;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is PostalCode))
                return false;

            return Equals((PostalCode)obj);
        }

        /// <summary>
        /// Equalses the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        public bool Equals(PostalCode other)
        {
            if (!other.Set || !this.Set)
            {
                return false;
            }

            if (other.NumericStreetCode.HasValue && this.NumericStreetCode.HasValue &&
                other.NumericStreetCode.Value != this.NumericStreetCode.Value)
            {
                return false;
            }

            if (other.NumericBoxCode.HasValue && this.NumericBoxCode.HasValue &&
                other.NumericBoxCode.Value != this.NumericBoxCode.Value)
            {
                return false;
            }

            return other.SearchableTown.Equals(this.SearchableTown, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator ==(PostalCode first, PostalCode second)
        {
            return first.Equals(second);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator !=(PostalCode first, PostalCode second)
        {
            return !first.Equals(second);
        }

        public bool Set { get; set; }
        public string Town { get; set; }
        public string BoxCode { get; set; }
        public string StreetCode { get; set; }
        public string City { get; set; }
        public string SearchableTown { get; set; }
        public string SearchableCity { get; set; }
        public int? NumericStreetCode { get; set; }
        public int? NumericBoxCode { get; set; }
        public string DisplayPostalCode { get; set; }

        public void SetValues(string town, string boxCode, string streetCode, string city)
        {
            DisplayPostalCode = string.IsNullOrWhiteSpace(streetCode) ? boxCode : streetCode;
            Town = town;
            SearchableTown = town.ToLowerInvariant();
            BoxCode = boxCode;
            StreetCode = streetCode;
            City = city;
            SearchableCity = string.IsNullOrWhiteSpace(city) ? string.Empty : city.ToLowerInvariant();
            var parsedStreetCode = 0;
            if (Int32.TryParse(streetCode, out parsedStreetCode))
            {
                NumericStreetCode = parsedStreetCode;
            }

            var parsedBoxCode = 0;
            if (Int32.TryParse(streetCode, out parsedBoxCode))
            {
                NumericBoxCode = parsedBoxCode;
            }

            Set = true;
        }
    }
}
