﻿// JSON C# Class Generator
// http://at-my-window.blogspot.com/?page=json-class-generator

using System;
using Newtonsoft.Json.Linq;
using JsonCSharpClassGenerator;

namespace PostalCodes.Models.JsonTypes
{

    internal class Northeast2
    {

        private JObject __jobject;
        public Northeast2(JObject obj)
        {
            this.__jobject = obj;
        }

        public double Lat
        {
            get
            {
                return JsonClassHelper.ReadFloat(JsonClassHelper.GetJToken<JValue>(__jobject, "lat"));
            }
        }

        public double Lng
        {
            get
            {
                return JsonClassHelper.ReadFloat(JsonClassHelper.GetJToken<JValue>(__jobject, "lng"));
            }
        }

    }
}
