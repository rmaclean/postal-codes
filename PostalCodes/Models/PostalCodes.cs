﻿


namespace PostalCodes.Models
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Device.Location;
    using System.Threading;

    public class PostalCodes : IEnumerable<PostalCode>, IDisposable
    {
        private PostalCode[] postalCodes;
        private bool ready;
        private static readonly PostalCodes instance = new PostalCodes();
        private GeoCoordinateWatcher gps = new GeoCoordinateWatcher();
        private GoogleAddressResolver addressResolver = new GoogleAddressResolver();

        public event Action GotPostalCodeFilter;

        public int PostalCodesFilter { get; private set; }
        public bool UsePostalCodeFilter { get; private set; }
        public string City { get; private set; }

        public static PostalCodes Instance
        {
            get
            {
                return instance;
            }
        }

        public void Init()
        {
            //does nothing really ;)
        }

        private PostalCodes()
        {
            ready = false;

            addressResolver.AddressResolutionComplete += (address) =>
                {
                    var filter = 0;
                    if (Int32.TryParse(address.PostalCode, out filter))
                    {
                        this.City = address.City;
                        PostalCodesFilter = filter;
                        UsePostalCodeFilter = true;
                        if (GotPostalCodeFilter != null)
                        {
                            GotPostalCodeFilter();
                        }
                    }
                };

            gps.PositionChanged += (s, e) =>
                {
                    addressResolver.Resolve(e.Position.Location);
                };

            gps.Start();

            ThreadPool.QueueUserWorkItem(state =>
                {
                    var lines = PostalCodeData.Raw.Data.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    postalCodes = new PostalCode[lines.Length];
                    string[] line;
                    for (int counter = 0; counter < lines.Length; counter++)
                    {
                        line = lines[counter].Split(';');
                        postalCodes[counter].SetValues(line[0], line[1], line[2], line[3]);
                    }

                    ready = true;
                });            
        }

        public IEnumerator<PostalCode> GetEnumerator()
        {
            while (!ready)
            {
                Thread.SpinWait(0);
            }

            return new PostalCodesEnumerator(postalCodes);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        private class PostalCodesEnumerator : IEnumerator<PostalCode>
        {
            private int position = -1;
            private PostalCode[] postalCodes;

            public PostalCodesEnumerator(PostalCode[] postalCodes)
            {
                this.postalCodes = postalCodes;
            }

            public PostalCode Current
            {
                get
                {
                    return postalCodes[position];
                }
            }

            public void Dispose()
            {
                // not needed
            }

            object IEnumerator.Current
            {
                get
                {
                    return this.Current;
                }
            }

            public bool MoveNext()
            {
                position++;
                return position < postalCodes.Length;
            }

            public void Reset()
            {
                position = -1;
            }
        }


        #region IDisposable Members

        /// <summary>
        /// Internal variable which checks if Dispose has already been called
        /// </summary>
        private Boolean disposed;

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(Boolean disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                if (gps != null)
                {
                    gps.Stop();
                    gps.Dispose();
                }
            }

            disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Call the private Dispose(bool) helper and indicate 
            // that we are explicitly disposing
            this.Dispose(true);

            // Tell the garbage collector that the object doesn't require any
            // cleanup when collected since Dispose was called explicitly.
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
