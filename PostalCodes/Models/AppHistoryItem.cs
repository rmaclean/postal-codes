﻿
namespace PostalCodes.Models
{

    public class AppHistoryItem
    {
        public string Version { get; private set; }
        public string Details { get; private set; }

        public AppHistoryItem(string version, string details)
        {
            this.Version = version;
            this.Details = details;
        }
    }

    public class AppHistory
    {
        public AppHistoryItem[] AppHistoryItems { get; private set; }

        public AppHistory()
        {
            AppHistoryItems = new[]{               
                new AppHistoryItem("Version 1.3", @"Bug fixes."),
                new AppHistoryItem("Version 1.2", @"Massively improved the UI (lot more Metro feel).
Use your GPS to find where you are and then use that to improve the search results.
Improved the no results messaging.
Improved start up times.
Fixed up the iconography.
Fixed up legal info."),
                new AppHistoryItem("Version 1.1", @"Added watermark to the query really to help with new users.
Improved the searching a lot - first shows a starts with, secondly a contains
Added the bugsense code
Disable the button & show progress bar during search
Switched to AtomicPhoneMVVM on Nuget
Pressing enter on the keyboard kicks off the search
Removed the ABC postal code"),
                new AppHistoryItem("Version 1.0", "Everything is new :)")
            };
        }
    }

}
