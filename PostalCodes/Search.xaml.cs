﻿
namespace PostalCodes
{
    using System.Windows.Input;
    using AtomicPhoneMVVM;
    using Microsoft.Phone.Controls;
    using System.Windows.Controls;
    using PostalCodes.Behaviours;
    using System.Windows.Interactivity;
    using SADev.WP;

    public partial class Search : PhoneApplicationPage
    {
        public Search()
        {
            InitializeComponent();
            this.BindData<ViewModels.Search>();
            var watermark = Interaction.GetBehaviors(queryTextBox)[0] as Watermark;
            watermark.SetWatermarkText();
            LittleWife.Nag("Postal Codes");
        }

        private void QueryTextboxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var textbox = sender as TextBox;
                var model = this.DataContext as ViewModels.Search;
                model.Query = textbox.Text;
                model.Go();
            }
        }        
    }
}