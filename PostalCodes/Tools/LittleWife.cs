﻿namespace SADev.WP
{
    using System.IO.IsolatedStorage;
    using System.Windows;
    using Microsoft.Phone.Tasks;

    public static class LittleWife
    {
        public static void Nag(string appName)
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains("RunCount"))
            {
                IsolatedStorageSettings.ApplicationSettings["RunCount"] = 0;
            }

            var runCount = (int)IsolatedStorageSettings.ApplicationSettings["RunCount"];
            runCount++;

            if (runCount == 5)
            {
                if (MessageBox.Show(string.Format("Do you enjoy using {0}? Please take a quick minute to rate it...", appName), appName, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    new MarketplaceReviewTask().Show();
                }
                else
                {
                    // didn't rate this time.
                    runCount = 0;
                }
            }

            IsolatedStorageSettings.ApplicationSettings["RunCount"] = runCount;
        }
    }
}