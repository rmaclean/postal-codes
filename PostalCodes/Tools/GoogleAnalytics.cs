﻿
namespace SADev.Tools
{
    using System;
    using System.ComponentModel.Composition.Hosting;
    using System.Windows;
    using Coding4Fun.Phone.Controls.Data;
    using Google.WebAnalytics;
    using Microsoft.Phone.Info;
    using Microsoft.WebAnalytics;
    using Microsoft.WebAnalytics.Behaviors;
    using Microsoft.WebAnalytics.Data;

    public class AppInfo : IApplicationService
    {
        private readonly IApplicationService _innerService;
        private readonly GoogleAnalytics _googleAnalytics;

        public AppInfo()
        {
            _googleAnalytics = new GoogleAnalytics();
            _googleAnalytics.CustomVariables.Add(new PropertyValue { PropertyName = "Device ID", Value = AnalyticsProperties.DeviceId });
            _googleAnalytics.CustomVariables.Add(new PropertyValue { PropertyName = "Application Version", Value = AnalyticsProperties.ApplicationVersion });
            _googleAnalytics.CustomVariables.Add(new PropertyValue { PropertyName = "Device OS", Value = AnalyticsProperties.OsVersion });
            _googleAnalytics.CustomVariables.Add(new PropertyValue { PropertyName = "Device", Value = AnalyticsProperties.Device });
            _innerService = new WebAnalyticsService
                                {
                                    IsPageTrackingEnabled = false,
                                    Services = { _googleAnalytics, }
                                };
        }

        public string WebPropertyId
        {
            get { return _googleAnalytics.WebPropertyId; }
            set { _googleAnalytics.WebPropertyId = value; }
        }


        public void StartService(ApplicationServiceContext context)
        {
            CompositionHost.Initialize(
                new AssemblyCatalog(
                    Application.Current.GetType().Assembly),
                new AssemblyCatalog(typeof(AnalyticsEvent).Assembly),
                new AssemblyCatalog(typeof(TrackAction).Assembly));
            _innerService.StartService(context);
        }

        public void StopService()
        {
            _innerService.StopService();
        }
    }

    public static class AnalyticsProperties
    {
        public static string DeviceId
        {
            get
            {
                var value = (byte[])DeviceExtendedProperties.GetValue("DeviceUniqueId");
                return Convert.ToBase64String(value);
            }
        }

        public static string DeviceManufacturer
        {
            get { return DeviceExtendedProperties.GetValue("DeviceManufacturer").ToString(); }
        }

        public static string DeviceType
        {
            get { return DeviceExtendedProperties.GetValue("DeviceName").ToString(); }
        }

        public static string Device
        {
            get { return string.Format("{0} - {1}", DeviceManufacturer, DeviceType); }
        }

        public static string OsVersion
        {
            get { return string.Format("WP {0}", Environment.OSVersion.Version); }
        }

        public static string ApplicationVersion
        {
            get { return PhoneHelper.GetAppAttribute("Version").Replace(".0.0", ""); }
        }
    }
}
