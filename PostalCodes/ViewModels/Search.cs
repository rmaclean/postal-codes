﻿

namespace PostalCodes.ViewModels
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using AtomicPhoneMVVM;
    using PostalCodes.Models;
    using Microsoft.Phone.Controls;
    using Microsoft.Phone.Tasks;
    using System;
    using System.Collections.Generic;

    public class Search : CoreData
    {
        private string query;
        private PostalCode selected;
        private bool noResults;
        private bool inProgress;
        private string lastQuery;

        public bool InProgress
        {
            get { return inProgress; }
            set
            {
                if (value != inProgress)
                {
                    inProgress = value;
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            RaisePropertyChanged("InProgress");
                        });
                }
            }
        }

        public bool NoResults
        {
            get { return noResults; }
            set
            {
                if (value != noResults)
                {
                    noResults = value;
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            RaisePropertyChanged("NoResults");
                        });
                }
            }
        }

        public PostalCode Selected
        {
            get { return selected; }
            set
            {
                if (value != selected)
                {
                    selected = value;
                    RaisePropertyChanged("Selected");
                }
            }
        }

        public ObservableCollection<PostalCode> Results { get; set; }
        public string Query
        {
            get { return query; }
            set
            {
                if (value != query)
                {
                    query = value;
                    RaisePropertyChanged("Query");
                }
            }
        }

        private bool canUseGoogleInfo;

        public bool CanUseGoogleInfo
        {
            get { return canUseGoogleInfo; }
            set
            {
                if (value != canUseGoogleInfo)
                {
                    canUseGoogleInfo = value;
                    RaisePropertyChanged("CanUseGoogleInfo");
                }
            }
        }

        public Search()
        {
            Results = new ObservableCollection<PostalCode>();
            NoResults = false;

            PostalCodes.Instance.GotPostalCodeFilter += () =>
                {
                    CanUseGoogleInfo = PostalCodes.Instance.UsePostalCodeFilter;
                };
        }

        [ReevaluateProperty("CanUseGoogleInfo")]
        public bool CanHere()
        {
            return CanUseGoogleInfo;
        }

        [AppBarCommand("here")]
        public void Here()
        {
            this.Query = PostalCodes.Instance.City;
            this.Go();
        }

        [AppBarCommand("about")]
        public void About()
        {
            this.Navigate("/About.xaml");
        }

        [AppBarCommand("google terms & conditions")]
        public void GoogleTerms()
        {
            var browser = new WebBrowserTask();
            browser.Uri = new Uri("https://developers.google.com/maps/terms");
            browser.Show();
        }

        public void Go()
        {
            SearchPostalCodes(this.Results, true, 100);
        }

        private void SearchPostalCodes(ObservableCollection<PostalCode> resultCollection, bool enforceLimit = false, int limit = 0)
        {
            if (string.IsNullOrWhiteSpace(this.Query))
            {
                return;
            }

            var searchQuery = this.Query.ToLowerInvariant();
            if (searchQuery == lastQuery)
            {
                return;
            }

            InProgress = true;
            System.Threading.ThreadPool.QueueUserWorkItem(state =>
                {
                    lastQuery = searchQuery;

                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            resultCollection.Clear();
                        });

                    IEnumerable<PostalCode> results = from p in PostalCodes.Instance
                                                      where p.SearchableTown.StartsWith(searchQuery)
                                                      orderby p.Town, p.City
                                                      select p;

                    if (PostalCodes.Instance.UsePostalCodeFilter)
                    {
                        IEnumerable<PostalCode> priorityResults = from p in results
                                                                  where (p.NumericStreetCode.HasValue &&
                                                                           ((p.NumericStreetCode >= PostalCodes.Instance.PostalCodesFilter - 100) &&
                                                                            (p.NumericStreetCode <= PostalCodes.Instance.PostalCodesFilter + 100))) ||
                                                                        (p.NumericBoxCode.HasValue &&
                                                                           ((p.NumericBoxCode >= PostalCodes.Instance.PostalCodesFilter - 100) &&
                                                                            (p.NumericBoxCode <= PostalCodes.Instance.PostalCodesFilter + 100)))
                                                                  select p;

                        var secondaryOriginalResults = results.Except(priorityResults);

                        results = priorityResults.Concat(secondaryOriginalResults);
                    }

                    IEnumerable<PostalCode> secondaryResults = from p in PostalCodes.Instance
                                            where (!string.IsNullOrWhiteSpace(p.SearchableCity) && p.SearchableCity.Contains(searchQuery)) ||
                                                  p.SearchableTown.Contains(searchQuery)
                                            orderby p.Town, p.City
                                            select p;

                    var uniqueSecondaryResults = secondaryResults.Except(results);

                    results = results.Concat(uniqueSecondaryResults);

                    if (enforceLimit)
                    {
                        results = results.Take(limit);
                    }

                    if (results.Any())
                    {
                        NoResults = false;
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            foreach (var item in results)
                            {
                                resultCollection.Add(item);
                            }
                        });
                    }
                    else
                    {
                        NoResults = true;
                    }

                    InProgress = false;
                });
        }
    }
}
