﻿
namespace PostalCodes.ViewModels
{
    using System;
    using AtomicPhoneMVVM;
    using Microsoft.Phone.Tasks;
    using PostalCodes.Models;

    public class About : CoreData
    {
        public AppHistoryItem[] History { get; set; }
        public string Legal { get; set; }

        public About()
        {
            var history = new AppHistory();
            History = history.AppHistoryItems;
            Legal = Data.PostalCodes.Legal;
        }

        public void MoreApps()
        {
            try
            {
                new MarketplaceSearchTask()
                {
                    ContentType = MarketplaceContentType.Applications,
                    SearchTerms = "rmaclean"
                }.Show();
            }
            catch (InvalidOperationException ex)
            {
                // multiple nav error
                if (!ex.Message.Contains("-2147220990"))
                {
                    throw;
                }
            }
        }

        public void Rate()
        {
            try
            {
                new MarketplaceReviewTask().Show();
            }
            catch (InvalidOperationException ex)
            {
                // multiple nav error
                if (!ex.Message.Contains("-2147220990"))
                {
                    throw;
                }
            }
        }

        public void Twitter()
        {
            try
            {
                var browser = new WebBrowserTask()
                {
                    Uri = new Uri("http://twitter.com/rmaclean", UriKind.Absolute)
                };

                browser.Show();
            }
            catch (InvalidOperationException ex)
            {
                // multiple nav error
                if (!ex.Message.Contains("-2147220990"))
                {
                    throw;
                }
            }
        }
    }
}
